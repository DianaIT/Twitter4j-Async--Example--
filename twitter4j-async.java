package tw;
import twitter4j.*;
import static twitter4j.TwitterMethod.*;

 

public class asinc {
    private static final Object LOCK = new Object();

    public static void main(String[] args) throws InterruptedException, TwitterException {
         ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
          .setOAuthConsumerKey("**********************")
          .setOAuthConsumerSecret("****************************")
          .setOAuthAccessToken("****************************************")
          .setOAuthAccessTokenSecret("*************************************+");
         AsyncTwitterFactory tf = new AsyncTwitterFactory(cb.build());
        AsyncTwitter twitter = tf.getInstance();
        long myID = twitter.getId();
        System.out.println(myID);
        twitter.addListener(new TwitterAdapter() {
            @Override
            public void searched(QueryResult queryResult) {
            
                for (Status status : queryResult.getTweets()) {
        System.out.println("@" + status.getUser().getScreenName());
    }
                synchronized (LOCK) {
                    LOCK.notify();
                }
            }
            
            public void gotMentions(ResponseList<Status> statuses){
       for (Status status : statuses) {
           System.out.println("@" + status.getUser().getScreenName() + " - " + status.getText());
       }
           
            }
           

            @Override
            public void onException(TwitterException e, TwitterMethod method) {
                if (method == SEARCH) {
                    e.printStackTrace();
                    synchronized (LOCK) {
                        LOCK.notify();
                    }
                } else {
                    synchronized (LOCK) {
                        LOCK.notify();
                    }
                    throw new AssertionError("Should not happen");
                }
            } 
        });
        twitter.getMentions();
         System.out.println("Fin acción 1");
        Query query = new Query("ejemplo"); //Entre paréntesis y comillas la palabra a buscar
         twitter.search(query);
         System.out.println("Fin acción 2");
         System.out.println("Fin app");// Muestras por consola para comprobar que es asíncrono
         
        synchronized (LOCK) {
            LOCK.wait();
            
        }
       
    }

}
